## Synopsis

Carousel is a Chrome Extension which rotates through all normal tabs in the initial window. It allows for a custom rotation speed as well as options to set a specific time spent on each tab.

## Motivation

This was built as a first attempt at building a Chrome extension. There was an existing library available with similar functionality which served as my inspiration, but the idea was simple enough I figured it would be more helpful to start from scratch and learn about Chrome extensions in general.

## Usage

The Carousel is started and stopped by clicking the extension icon in the top right. The rotation timing is customized on the options page. There will be an option available for all normal tabs that are open when options is opened. When the active tab is chrome tab (extensions page, options, new tab, etc.) the carousel automatically pauses.